<div>
    <div class="flex justify-center w-max">
        <div class="flex flex-col w-max">

{{--            <div class="flex justify-between pt-16 ">--}}
{{--                <h1>Planningsbord</h1>--}}
{{--                <x-button.circle primary label="+" wire:click="modalTrue"/>--}}
{{--            </div>--}}

            <div class="mt-4 mb-4 flex justify-between items-center">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Bord
                </h3>
                <div>
                    <x-ori-link.circle
                        wire:click="modalTrue"
                    />
                </div>
            </div>

            <div class="flex flex-row my-10">
                <div class="flex flex-row space-x-5" wire:sortable-group="updateOrder" wire:sortable="updateGroupOrder">
                    @foreach($statuses as $status)
                        @include('laravel-planningboards::livewire.planning-board.column', ['table' => $status->id])
                    @endforeach
                </div>
                @livewire('laravel-planningboards::create-issue')
                @livewire('laravel-planningboards::edit-issue')
            </div>
        </div>
    </div>
</div>
