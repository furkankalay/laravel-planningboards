<div>
    <x-modal.card title="Voeg een taak toe" blur wire:model.defer="addTaskModal">
        <form>
            @csrf
            <div class="grid grid-cols-1 sm:grid-cols-6 gap-4">
                <div class="col-span-1 sm:col-span-4">
                    <x-input label="Titel*" placeholder="De titel" wire:model="title"/>
                </div>
                <div class="col-span-1 sm:col-span-2">
                    <x-select
                        name="priority"
                        label="Prioriteit*"
                        placeholder="Kies een prioriteit"
                        wire:model="priority"
                    >
                        <x-select.option label="Hoog" value="1"/>
                        <x-select.option label="Medium" value="2"/>
                        <x-select.option label="Laag" value="3"/>
                    </x-select>
                </div>

                <div class="col-span-1 sm:col-span-7">
                    <x-textarea name="description" label="Beschrijving*" placeholder="Beschrijving van het taak" wire:model="description"/>
                </div>

                <div class="col-span-1 sm:col-span-3">
                    <x-select
                        name="executor"
                        label="Uitvoerder"
                        :async-data="route('api.users.index')"
                        placeholder="Kies een uitvoerder"
                        wire:model.defer="executor_id"
                        option-label="name"
                        option-value="id"
                    >
                    </x-select>
                </div>
                <div class="col-span-1 sm:col-span-3">
                    <x-input label="Uur" name="execute_time" placeholder="Uren benodigd" wire:model="execute_time"/>
                </div>
            </div>

            <x-slot name="footer">
                <div class="flex w-full justify-end">
                    <x-button flat label="Annuleer" x-on:click="close"/>
                    <x-button primary label="Voeg toe" name="submit" wire:click.prevent="store()" type="submit"/>
                </div>
            </x-slot>
        </form>
    </x-modal.card>
</div>
