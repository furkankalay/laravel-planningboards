<div>
    <x-modal.card title="Bewerk een taak" blur wire:model.defer="updateModal" x-on:close="$wire.close()"
    >

        <div x-data="{ editMode: @entangle('editMode') }">
            <div class="grid grid-cols-1 sm:grid-cols-6 gap-4" x-show="editMode">
                <div class="col-span-1 sm:col-span-4 drop-shadow-none">
                    <x-input label="Titel" placeholder="De titel" wire:model.defer="title"/>
                </div>
                <div class="col-span-1 sm:col-span-2">

                    <x-select
                        label="Prioriteit"
                        placeholder="Kies een prioriteit"
                        :options="[
                                        ['name' => 'Hoog',  'id' => 1],
                                        ['name' => 'Medium', 'id' => 2],
                                        ['name' => 'Laag',   'id' => 3],
                                    ]"
                        option-label="name"
                        option-value="id"
                        wire:model.defer="priority"
                    />

                </div>

                <div class="col-span-1 sm:col-span-7">
                    <x-textarea wire:model.defer="description" label="Beschrijving"
                                placeholder="Beschrijving van het taak"/>
                </div>

                <div class="col-span-1 sm:col-span-4">
                    <x-select
                        name="executor"
                        label="Uitvoerder"
                        :async-data="route('api.users.index')"
                        placeholder="Kies een uitvoerder"
                        wire:model.defer="executor_id"
                        option-label="name"
                        option-value="id"
                    >
                    </x-select>
                </div>
                <div class="col-span-1 sm:col-span-1">
                    <x-input label="Uur" placeholder="Uren benodigd" wire:model.defer="execute_time"/>
                </div>
                <div class="col-span-1 sm:col-span-1">
                    <x-input label="Versie" placeholder="Versie" wire:model.defer="version_release"/>
                </div>


            </div>

            <form x-show="!editMode">
                @csrf
                <div class="grid grid-cols-1 sm:grid-cols-6 gap-4 gap-y-8">


                    <div class="col-span-1 sm:col-span-4 drop-shadow-none">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Titel</div>
                        <div class="text-sm font-sm text-gray-700 pt-3">{{$title}}</div>
                    </div>
                    <div class="col-span-1 sm:col-span-2">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Prioriteit</div>
                        <div class="text-sm font-sm text-gray-700 pt-3">{{$priority_name}}</div>
                    </div>
                    <div class="col-span-1 sm:col-span-7">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Beschrijving</div>
                        <div class="text-sm font-sm text-gray-700 pt-3 whitespace-pre-line">{{$description}}</div>
                    </div>
                    <div class="col-span-1 sm:col-span-2">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Maker</div>
                        <div class="text-sm font-sm text-gray-700 pt-3">{{$selectedUserName}}</div>
                    </div>
                    <div class="col-span-1 sm:col-span-2">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Uitvoeder</div>
                        <div class="text-sm font-sm text-gray-700 pt-3">{{$selectedExecutorName}}</div>
                    </div>
                    <div class="col-span-1 sm:col-span-1">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Uur</div>
                        <div class="text-sm font-sm text-gray-700 pt-3">{{$execute_time}}</div>
                    </div>
                    <div class="col-span-1 sm:col-span-1">
                        <div class="text-sm font-medium text-gray-700 dark:text-gray-400">Versie</div>
                        <div class="text-sm font-sm text-gray-700 pt-3">{{$version_release}}</div>
                    </div>


                </div>
            </form>
        </div>

        <x-slot name="footer">
            <div class="flex justify-between gap-x-4">
                <x-button flat negative label="Verwijder" wire:click="delete"/>
                <div class="flex space-between-4">
                    <div class="flex items-center text-gray-500 font-light text-xs pr-3">
                        WAT-{{$selectedIssueId}}
                    </div>
                    <div class="flex items-center text-gray-500 font-light text-xs pl-3">
                        {{$created_at}}
                    </div>
                </div>
                <div class="flex">
                    <x-button flat label="Annuleer" wire:click="close" x-on:click="close"/>
                    <x-button primary label="{{ $editMode ? 'Wijzig' : 'Bewerk' }}"
                              wire:click="{{ $editMode ? 'update' : 'editModeOn' }}"/>
                </div>
            </div>
        </x-slot>

    </x-modal.card>

    <x-modal wire:model.defer="versionModal" x-on:close="$wire.close()">
        <x-card title="Voer een versie in">
            <div>

                <x-input label="Versie" placeholder="Versie" wire:model.defer="version_release"/>

            </div>

            <x-slot name="footer">
                <div class="flex justify-end gap-x-4">
                    <x-button flat label="Annuleer" x-on:click="close" />
                    <x-button primary label="Wijzig"  wire:click="updateRelease"/>
                </div>
            </x-slot>
        </x-card>
    </x-modal>
</div>
