<div>
    <div class="flex flex-col w-max" style="height: 630px;">
        <div class="flex w-72 flex-row overflow-hidden shadow border-b border-gray-200">
            <div class="flex flex-col w-full divide-y divide-gray-200 ">
                <div
                    class="h-12 min-h-[3rem] bg-gray-50 w-full px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider w-2/5 flex justify-center items-center">
                    {{$status->name}}
                </div>
                <div class="divide-y divide-gray-200 w-max overflow-auto"
                     wire:sortable-group.item-group="{{$table}}"
                     wire:drop="itemDropped({{ $table }})">
                    @if($issues->where('status_id', $table)->isNotEmpty())

                        @foreach($issues->where('status_id', $table) as $issue)

                            <div
                                class="flex flex-col bg-white hover:bg-gray-50 px-6 py-3 h-32 w-72 justify-between cursor-pointer"
                                wire:click="edit({{$issue}})"
                                wire:sortable-group.item="{{ $issue->id }}"
                                wire:key="issue-{{ $issue->id }}">
                                <div class="flex flex-row justify-between items-center">
                                    <div
                                        class="py-1 text-base leading-5 font-medium text-gray-900 w-36">{{$issue->title}}</div>
                                    <div class="text-gray-500 font-light text-xs/[13.64px]" wire:model="issueId" value="{{ $issue->id }}">
                                        WAT-{{$issue->id}}</div>

                                    <span
                                        class="h-4 w-4 bg-{{$issue->priority->getColor()}}-100 flex items-center justify-center rounded-full"
                                        aria-hidden="true">
                                                   <span class="h-2 w-2 bg-{{$issue->priority->getColor()}}-400 rounded-full"></span>
                                    </span>

                                </div>
                                <div
                                    class="py-1 text-sm leading-5 text-gray-500 line-clamp-2">{{$issue->description}}
                                </div>
                                <div class="flex justify-between">
                                    <div
                                        class="py-1 block text-xs leading-5 text-gray-700 w-36">{{$issue->user->name}}
                                    </div>
                                    @if($issue->version_release && $table === 4)
                                        <div
                                            class="py-1 block text-xs leading-5 text-gray-700">{{$issue->version_release}}
                                        </div>
                                    @endif
                                    <div
                                        class="py-1 block text-xs leading-5 text-gray-700 w-5">{{$issue->execute_time}}
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @else
                        <div
                            class="flex flex-col bg-white hover:bg-gray-50 px-6 py-3 w-72 justify-between cursor-pointer"

                        >
                            <div class="flex flex-row justify-between items-center">
                                <div
                                    class="py-1 text-sm leading-5 text-orange-600"
                                    wire:click="modalTrue">
                                    + Voeg een taak toe
                                </div>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
