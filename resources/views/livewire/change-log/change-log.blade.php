<div>
    <div class="relative ">
        <div class="flex flex-col ">

            <div class="flex justify-start mt-4 ">
                <h3 class="text-lg leading-6 font-medium text-gray-900">Logboek</h3>
            </div>
            <div class="flex flex-col mt-4 space-y-6">


                @foreach($singleDotIssues as $version => $versionLog)
                    @include('laravel-planningboards::livewire.change-log.logs.version-log',['versionLog' => $versionLog, 'version' => $version])
                @endforeach



            </div>
        </div>
    </div>
</div>
