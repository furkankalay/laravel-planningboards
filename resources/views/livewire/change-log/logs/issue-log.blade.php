<div>
    <div class="flex flex-col">

        <div class="py-1 text-base leading-5 font-medium text-gray-900 ">{{$issueLog['title']}}</div>
        <div class="py-1 text-sm leading-5 text-gray-500 line-clamp-3 ml-2 whitespace-pre-line">{{$issueLog['description']}}</div>
    </div>
</div>
