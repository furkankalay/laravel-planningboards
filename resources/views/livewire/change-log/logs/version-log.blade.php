<div>

    <div class="flex flex-col">
        <div class="flex flex-row border-b-2 border-orange-400  mb-3 space-x-3">
            <div class="py-1 text-base leading-5 font-medium text-gray-900 text-xl">{{$version}}</div>
        </div>
        <div class="space-y-3">
            @foreach($versionLog as $version => $issueLog)
              @include('laravel-planningboards::livewire.change-log.logs.issue-log', ['issueLog' => $issueLog, 'version' => $version])
            @endforeach
        </div>
    </div>
</div>
