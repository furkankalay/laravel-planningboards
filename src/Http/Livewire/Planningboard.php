<?php

namespace Furkankalay\LaravelPlanningboards\Http\Livewire;

use App\Models\User;
use Furkankalay\LaravelPlanningboards\Enums\Priorities;
use Furkankalay\LaravelPlanningboards\Models\Issue;
use Furkankalay\LaravelPlanningboards\Models\IssueStatus;
use Furkankalay\LaravelPlanningboards\Models\Priority;
use Livewire\Component;

class Planningboard extends Component
{

    public $itemOrder, $issueOrder;


    protected $listeners = [
        'AddedRelease' => 'handleAddedRelease',
        ];

    public function modalTrue(){
       $this->emit('AddEvent');
    }

    // function to update the orders for the sortable package
    public function updateOrder($list){
        $issues = Issue::all();

        foreach($list as $items) {
            $column_id = $items['order'];

            foreach($items['items'] as $item){
                $issue = $issues->find($item['value']);


                if ($issue->status_id != $column_id && $column_id == 4 && $issue->version_release == '') {
                    $this->itemOrder = $item;
                    $this->issueOrder = $issue;
                    $this->itemDropped($issue);

                }else{
                    $issue->update(['order_position' => $item['order']]);
                    $issue->update(['status_id' => $column_id]);
                }
            }
        }
    }

    public function handleAddedRelease(){

        $this->issueOrder->update(['order_position' => $this->itemOrder['order']]);
        $this->issueOrder->update(['status_id' => 4]);
    }

    public function itemDropped(Issue $issueRelease)
    {
        $this->emit('updateRelease', $issueRelease);
    }

    public function edit(Issue $issue)
    {
        $this->emit('viewEvent', $issue);
    }

    public function render()
    {
        return view('laravel-planningboards::livewire.planning-board.planningboard', [
            'issues' => Issue::orderBy('order_position')->get(),
            'statuses' => IssueStatus::all(),
            'users' => User::all(),
        ]);
    }
}



