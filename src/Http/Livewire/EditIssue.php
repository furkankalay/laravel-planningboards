<?php

namespace Furkankalay\LaravelPlanningboards\Http\Livewire;

use Furkankalay\LaravelPlanningboards\Models\Issue;
use Livewire\Component;

class EditIssue extends CreateIssue
{
    protected $listeners = [
        'editModeEvent' => 'handleEditModeOn',
        'viewEvent' => 'handleEdit',
        'updateRelease' => 'handleUpdateRelease',
        'some-event' => '$refresh',

    ];

    public function handleUpdateRelease(Issue $issueRelease){

        $this->selectedIssuesRelease = $issueRelease;


        $this->versionModal = true;
    }

    public function editModeOn(){

        $this->editMode = true;
    }

    public function handleEdit(Issue $issue)
    {

        if ($issue->executor_id)
        {
            $this->executor_id = $issue->executor->id;
            $this->selectedExecutorName = $issue->executor->name;
        }else{
            $issue->executor_id != null;
        }

        $this->selectedIssues = $issue;
        $this->selectedUserName = $issue->user->name;
        $this->selectedIssueId = $issue->id;
        $this->priority_name = $issue->priority->getText();
        $this->user_id = $issue->user->id;
        $this->title = $issue->title;
        $this->description = $issue->description;
        $this->priority = $issue->priority;
        $this->execute_time = $issue->execute_time;
        $this->version_release = $issue->version_release;
        $this->created_at = $issue->created_at->format('d-m-Y');
        $this->updateModal = true;

    }

    public function updateRelease()
    {
        $this->validate([
            'version_release' => 'required',
        ]);

        $issue = Issue::find($this->selectedIssuesRelease->id);
        $issue->update([
            'version_release' =>  $this->version_release,
        ]);

        $this->close();
        $this->emit('AddedRelease');
        $this->emit('some-event');
        $this->clearVersion();

    }

    public function update()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
            'priority' => 'required',
            'user_id' => 'required',
        ]);

        $issue = Issue::find($this->selectedIssues->id);
        $issue->update([
            'title' => $this->title,
            'description' => $this->description,
            'priority' =>  $this->priority,
            'user_id' =>  $this->user_id,
            'executor_id' =>  $this->executor_id,
            'execute_time' =>  $this->execute_time,
            'version_release' =>  $this->version_release,
        ]);

        $this->close();
        return redirect('/changes/planningsboard/')->with('message', 'Taak is bijgewerkt!');

    }

    public function delete()
    {
        $this->close();
        Issue::find($this->selectedIssues->id)->delete();
        $this->clear();
    }

    public function render()
    {
        return view('laravel-planningboards::livewire.planning-board.create-edit.edit-issue');
    }
}
