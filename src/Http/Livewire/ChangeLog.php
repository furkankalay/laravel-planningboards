<?php

namespace Furkankalay\LaravelPlanningboards\Http\Livewire;

use Furkankalay\LaravelPlanningboards\Models\Issue;
use Livewire\Component;

class ChangeLog extends Component
{
    public $issues;

    public function mount()
    {
        $this->issues = Issue::all();
    }

        public function getSingleDotIssues(){
        return $this->issues->where('status_id', 4)->groupBy('version_release');
    }


        public function render()
        {
            $filteredIssues = $this->getSingleDotIssues();

            // Convert collections to arrays
            $filteredIssues = collect($filteredIssues)->map(function ($versionLogs) {
                return $versionLogs->toArray();
            })->toArray();

            uksort($filteredIssues, function ($version1, $version2) {
                $version1Parts = explode('.', $version1);
                $version2Parts = explode('.', $version2);

                $count1 = count($version1Parts);
                $count2 = count($version2Parts);

                for ($i = 0; $i < max($count1, $count2); $i++) {
                    $part1 = $i < $count1 ? (int)$version1Parts[$i] : 0;
                    $part2 = $i < $count2 ? (int)$version2Parts[$i] : 0;

                    if ($part1 > $part2) {  // Reverse the comparison
                        return -1;
                    } elseif ($part1 < $part2) {  // Reverse the comparison
                        return 1;
                    }
                }

                return 0;
            });
        return view('laravel-planningboards::livewire.change-log.change-log'
            , ['singleDotIssues' => $filteredIssues,]
        );
    }
}
