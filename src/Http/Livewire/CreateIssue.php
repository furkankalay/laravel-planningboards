<?php

namespace Furkankalay\LaravelPlanningboards\Http\Livewire;

use Furkankalay\LaravelPlanningboards\Models\Issue;
use Livewire\Component;

class   CreateIssue extends Component
{
    public  $priority_name, $title ,$description, $priority, $execute_time, $selectedIssues, $executor_id, $status_id, $user_id, $version_release, $issue, $selectedUserName, $selectedExecutorName, $selectedIssueId, $created_at, $selectedIssuesRelease;
    public bool $addTaskModal = false;
    public bool $updateModal = false;
    public bool $editMode = false;
    public bool $versionModal = false;

    protected $listeners = [
        'AddEvent' => 'handleAdd',
    ];

    public function handleAdd(){
        $this->addTaskModal = true;
    }

    public function close() {
        $this->updateModal = false;
        $this->editMode = false;
        $this->addTaskModal = false;
        $this->versionModal = false;
    }

    public function store()
    {
//        $randomNumber = random_int(1, 5);

        $validatedDate = $this->validate([
            'title' => 'required',
            'description' => 'required',
            'priority' => 'required',
        ]);
         $validatedDate['user_id'] = auth()->id();
//        $validatedDate['user_id'] = strval($randomNumber);
        $validatedDate['status_id'] = '1';

        Issue::create($validatedDate);
        return redirect('/changes/planningsboard/')->with('message', 'Taak is aangemaakt!');
    }

    public function clear() {
        $this->title = '';
        $this->description = '';
        $this->priority = '';
        $this->execute_time = '';
        $this->version_release = '';
        $this->status_id = '';
        $this->user_id = '';
        $this->issue = '';
        $this->selectedUserName = '';
        $this->selectedExecutorName= '';
        $this->executor_id = '';
        $this->created_at = '';
        $this->selectedIssueId= '';
        $this->priority_name= '';
        $this->selectedIssues = null;
    }
    public function clearVersion() {
        $this->version_release = '';
    }

    public function render()
    {
        return view('laravel-planningboards::livewire.planning-board.create-edit.create-issue');
    }
}
