<?php

namespace Furkankalay\LaravelPlanningboards\Factories;

use Furkankalay\LaravelPlanningboards\Models\Issue;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<Furkankalay\LaravelPlanningboards\Models\Issue>
 */

class IssueFactory extends Factory
{

    protected $model = Issue::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $userIDs = DB::table('users')->pluck('id');
        $issuesStatusIDs = DB::table('issue_statuses')->pluck('id');
        return [
            // 'user_id' => '1',
            // 'category_id' => '1',
            'user_id' => $this->faker->randomElement($userIDs),
            'executor_id' => $this->faker->randomElement($userIDs),
            'status_id' => $this->faker->randomElement($issuesStatusIDs),
            'title' => $this->faker->word(),
            'description' => $this->faker->paragraph(3),
            'version_release' => '4.13.1',
            'priority' => $this->faker->numberBetween(1,3),
            'execute_time' =>  '3h',
            'order_position' => '2',


        ];
    }
}
