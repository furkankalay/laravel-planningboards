<?php

namespace Furkankalay\LaravelPlanningboards\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;


use App\Models\User;
use Furkankalay\LaravelPlanningboards\Enums\Priorities;
use Furkankalay\LaravelPlanningboards\Factories\IssueFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    use HasFactory;

    protected $fillable = ['priority', 'title', 'description', 'execute_time','user_id', 'executor_id', 'status_id', 'version_release','order_position', 'created_at'];

    protected $casts = [
        'priority' => Priorities::class,
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function executor(){
        return $this->belongsTo(User::class);
    }

    public function status(){
        return $this->belongsTo(IssueStatus::class);
    }
    protected static function newFactory()
    {
        return IssueFactory::new();
    }
}
