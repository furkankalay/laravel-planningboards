<?php

namespace Furkankalay\LaravelPlanningboards\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Furkankalay\LaravelPlanningboards\Enums\Priorities;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class IssueStatus extends Model
{
    use HasFactory;

   public function issues(){
       return $this->hasMany(Issue::class);
   }
}
