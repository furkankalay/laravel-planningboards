<?php

namespace Furkankalay\LaravelPlanningboards\Enums;

enum Priorities : int {
    case HIGH = 1;
    case MEDIUM = 2;
    case LOW = 3;

    public function getText(): string{
        return match ($this){
            self::HIGH => 'Hoog',
            self::MEDIUM => 'Medium',
            self::LOW => 'Laag',
        };
    }

    public function getColor(): string{
        return match ($this){
            self::HIGH => 'red',
            self::MEDIUM => 'orange',
            self::LOW => 'green',
        };
    }
}

