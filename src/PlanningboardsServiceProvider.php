<?php

namespace Furkankalay\LaravelPlanningboards;




use Furkankalay\LaravelPlanningboards\Http\Livewire\ChangeLog;
use Furkankalay\LaravelPlanningboards\Http\Livewire\Columnboard;
use Furkankalay\LaravelPlanningboards\Http\Livewire\CreateIssue;
use Furkankalay\LaravelPlanningboards\Http\Livewire\EditIssue;
use Furkankalay\LaravelPlanningboards\Http\Livewire\Planningboard;
use Furkankalay\LaravelPlanningboards\Http\Livewire\Table;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\Livewire;

class PlanningboardsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->afterResolving(BladeCompiler::class, function () {
            Livewire::component('laravel-planningboards::planningboard', Planningboard::class);
            Livewire::component('laravel-planningboards::create-issue', CreateIssue::class);
            Livewire::component('laravel-planningboards::edit-issue', EditIssue::class);
            Livewire::component('laravel-planningboards::change-log', ChangeLog::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-planningboards');
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        if ($this->app->runningInConsole()) {

            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
            $this->loadFactoriesFrom(__DIR__ . '/../database/factories');

        }
    }
}
