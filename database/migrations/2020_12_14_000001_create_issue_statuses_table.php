<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{


    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $statusses = ['wensen','nog doen','actief','gereed'];

        Schema::create('issue_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
        });

        foreach ($statusses as $status){
            DB::table('issue_statuses')->insert([
                'name' => $status,
            ]);
        }


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('issue_statuses');
    }
};

