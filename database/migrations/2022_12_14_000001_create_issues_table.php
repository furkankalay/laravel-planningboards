<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->foreignId('status_id')->nullable()->references('id')->on('issue_statuses')->onDelete('set null');
            $table->integer('priority')->nullable();
            $table->foreignId('user_id')->nullable()->references('id')->on('users')->onDelete('set null');
            $table->foreignId('executor_id')->default(null)->nullable()->references('id')->on('users')->onDelete('set null');
            $table->string('execute_time')->nullable();
            $table->string('version_release')->nullable();
            $table->integer('order_position')->default(null)->nullable();

            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('issues');
    }
};
