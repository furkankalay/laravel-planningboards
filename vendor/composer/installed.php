<?php return array(
    'root' => array(
        'name' => 'furkankalay/laravel-planningboards',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '76670ac56d70f8b9a76fb6b2bb48d5d138789b0e',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'brick/math' => array(
            'pretty_version' => 'v0.11.x-dev',
            'version' => '0.11.9999999.9999999-dev',
            'reference' => '8d60a347dd96c2c748f8993300a27d079a53144a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../brick/math',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dflydev/dot-access-data' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'f41715465d65213d644d3141a6a93081be5d3549',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dflydev/dot-access-data',
            'aliases' => array(
                0 => '3.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '2.1.x-dev',
            'version' => '2.1.9999999.9999999-dev',
            'reference' => 'bf265da9f831e46e646c7a01b59ee8a33f8c0365',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '3.0.x-dev',
            'version' => '3.0.9999999.9999999-dev',
            'reference' => '84a527db05647743d50373e0ec53a152f2cde568',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dragonmantank/cron-expression' => array(
            'pretty_version' => 'v3.3.2',
            'version' => '3.3.2.0',
            'reference' => '782ca5968ab8b954773518e9e49a6f892a34b2a8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dragonmantank/cron-expression',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'egulias/email-validator' => array(
            'pretty_version' => '4.x-dev',
            'version' => '4.9999999.9999999.9999999-dev',
            'reference' => '97c28cd611278a5686b7f8b47f4136472bebbe1f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../egulias/email-validator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'fruitcake/php-cors' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'bb96f0246364ea36d4d22364a39023a7e2974fc8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fruitcake/php-cors',
            'aliases' => array(
                0 => '1.2.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'furkankalay/laravel-planningboards' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '76670ac56d70f8b9a76fb6b2bb48d5d138789b0e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => array(
            'pretty_version' => '1.1.x-dev',
            'version' => '1.1.9999999.9999999-dev',
            'reference' => '60c5f57bee20beb1a4a3cc5fe9170de4a64521d2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../graham-campbell/result-type',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/uri-template' => array(
            'pretty_version' => '1.0.x-dev',
            'version' => '1.0.9999999.9999999-dev',
            'reference' => 'a6a9904f03493b36f97f2f5cba20127d2e264bb3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/uri-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/auth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/broadcasting' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/bus' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/collections' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/conditionable' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/config' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/console' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/container' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/contracts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/database' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/encryption' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/events' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/filesystem' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/hashing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/macroable' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/mail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/notifications' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/pagination' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/pipeline' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/process' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/queue' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/redis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/routing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/session' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/support' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/testing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/translation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/validation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'illuminate/view' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '10.x-dev',
            ),
        ),
        'laravel/framework' => array(
            'pretty_version' => '10.x-dev',
            'version' => '10.9999999.9999999.9999999-dev',
            'reference' => '693e0c790ed61f78ac75cb1445df8362f6606a3c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'laravel/serializable-closure' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '5c1e047e38fee0cb8fbbe1ed4439ebfd96f64986',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/serializable-closure',
            'aliases' => array(
                0 => '1.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'league/commonmark' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '23a79e419dae0e650b3378b2e7845f54ed0f6659',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/commonmark',
            'aliases' => array(
                0 => '2.5.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'league/config' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'fbb4c4ab01d6d14a6db46923671bb1c2ee5b6691',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/config',
            'aliases' => array(
                0 => '1.2.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '3.x-dev',
            'version' => '3.9999999.9999999.9999999-dev',
            'reference' => 'be639d62a8ac69f17cac2d70d38415e3ee2cb721',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/flysystem-local' => array(
            'pretty_version' => '3.x-dev',
            'version' => '3.9999999.9999999.9999999-dev',
            'reference' => '543f64c397fefdf9cfeac443ffb6beff602796b3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem-local',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'livewire/livewire' => array(
            'pretty_version' => 'v2.12.3',
            'version' => '2.12.3.0',
            'reference' => '019b1e69d8cd8c7e749eba7a38e4fa69ecbc8f74',
            'type' => 'library',
            'install_path' => __DIR__ . '/../livewire/livewire',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'b05bf55097060ec20f49ccec0cf2f8e5aaa468b3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(
                0 => '3.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'mtdowling/cron-expression' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '^1.0',
            ),
        ),
        'nesbot/carbon' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'c1001b3bc75039b07f38a79db5237c4c529e04c8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(
                0 => '2.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'nette/schema' => array(
            'pretty_version' => 'v1.2.x-dev',
            'version' => '1.2.9999999.9999999-dev',
            'reference' => 'b6b3f41ea96ebf082657d271d9b1bac6071d7d29',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/schema',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nette/utils' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '0a94bd77ff1775e039aa4416abc9807d12c198ba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/utils',
            'aliases' => array(
                0 => '4.0.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'nunomaduro/termwind' => array(
            'pretty_version' => 'v1.15.1',
            'version' => '1.15.1.0',
            'reference' => '8ab0b32c8caa4a2e09700ea32925441385e4a5dc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nunomaduro/termwind',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoption/phpoption' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'dd3a383e599f49777d8b628dadbb90cae435b87e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoption/phpoption',
            'aliases' => array(
                0 => '1.9.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '90db7b9ac2a2c5b849fcb69dde58f3ae182c68f5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(
                0 => '2.0.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'psr/container-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.1|2.0',
            ),
        ),
        'psr/event-dispatcher' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'e275e2d67d53964a3f13e056886ecd769edee021',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/event-dispatcher',
            'aliases' => array(
                0 => '1.0.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'fe5ea303b0887d5caefd3d431c3e61ad47037001',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(
                0 => '3.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '3.0.0',
                1 => '1.0|2.0|3.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '2d280c2aaa23a120f35d55cfde8581954a8e77fa',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(
                0 => '3.0.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0|3.0',
            ),
        ),
        'ramsey/collection' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'reference' => 'a4b48764bfbb8f3a6a4d1aeb1a35bb5e9ecac4a5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/collection',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ramsey/uuid' => array(
            'pretty_version' => '4.x-dev',
            'version' => '4.9999999.9999999.9999999-dev',
            'reference' => 'bef858d57e5ede26bae2e2f2b51b21fd7f6e7590',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/uuid',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '4.x-dev',
            ),
        ),
        'symfony/console' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '845f20181c7f11ca9564490cc791c2fe91d443ef',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/css-selector' => array(
            'pretty_version' => '7.0.x-dev',
            'version' => '7.0.9999999.9999999-dev',
            'reference' => '6ba609e09b51517816da205fbc18b21656fded84',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/css-selector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '7c3aff79d10325257a001fcf92d991f24fc967cf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(
                0 => '3.4.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/error-handler' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => 'd7d32da07f73cc0742d1ffac9116e746bbeb26d0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/error-handler',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => '7.0.x-dev',
            'version' => '7.0.9999999.9999999-dev',
            'reference' => '6959079db63d7eec78bfafd319fde43dbc7562f4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'a76aed96a42d2b521153fb382d418e30d18b59df',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(
                0 => '3.4.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.0|3.0',
            ),
        ),
        'symfony/finder' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '1bb60aa99f06979e6078007a812eb7c5ffc8efc2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '86c012b4cc8dca1b03003ede87a44ffa2a15387b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-kernel' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '68bce8af8d54d21e49afc87fb3c329ba163cb187',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-kernel',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/mailer' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => 'ead2a4afe39229e3b6fccc8f1306ca710707301b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mailer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/mime' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '42ee631fe048298cc557f4aeea19c49123a9bced',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/mime',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'ea208ce43cbb04af6867b4fdddb1bdbf84cc28cb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '875e90aeea2777b6f135677f618529449334a612',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'ecaafce9f77234a6a449d29e49267ba10499116d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '8c4ad05dd0120b6a53c1ca374dca2ad0a1c4ed92',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'f9c7affe77a00ae32ca127ca6833d034e6d33f25',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '70f4aebd92afca2f865444d30a4d2151c13c3179',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '6caa57379c4aec19c0a12a38b59b26487dcfe4b5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php83' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '4d30d60ba762ce6792e014576b57fa48ad5c5a68',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php83',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-uuid' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '9c44518a5aff8da565c8a55dbe85d2769e6f630e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-uuid',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '8741e3ed7fe2e91ec099e02446fb86667a0f1628',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/routing' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '175a0b405c6e47b9f10e0bf5c4d7e03cf730065b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/routing',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '40da9cc13ec349d9e4966ce18b5fbcd724ab10a4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(
                0 => '3.4.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/string' => array(
            'pretty_version' => '7.0.x-dev',
            'version' => '7.0.9999999.9999999-dev',
            'reference' => '92292507f95a4cca4b0e54cb4eba1be730c8cf7a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => '238d73308a5851542ea1fef3561205c0bf1118a9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '02c24deb352fb0d79db5486c0c79905a85e37e86',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(
                0 => '3.4.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3|3.0',
            ),
        ),
        'symfony/uid' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => 'cfdc9354fa05db01e86ba18c8ed5bf93153d2fb0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/uid',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => '6.4.x-dev',
            'version' => '6.4.9999999.9999999-dev',
            'reference' => 'fee83bb21fcdc6d71aec5ec36c017a7368000584',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'tijsverkoyen/css-to-inline-styles' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '416a55b7b527db57e323f5fefa77e2492b64a0b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tijsverkoyen/css-to-inline-styles',
            'aliases' => array(
                0 => '2.2.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '1a7ea2afc49c3ee6d87061f5a233e3a035d0eae7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(
                0 => '5.5.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'voku/portable-ascii' => array(
            'pretty_version' => '2.0.1',
            'version' => '2.0.1.0',
            'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
            'type' => 'library',
            'install_path' => __DIR__ . '/../voku/portable-ascii',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'webmozart/assert' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webmozart/assert',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wireui/wireui' => array(
            'pretty_version' => 'v1.17.9',
            'version' => '1.17.9.0',
            'reference' => 'ef1502d9fd1dbf36e8eb36e3b1452b6f2f25fa1f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wireui/wireui',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
